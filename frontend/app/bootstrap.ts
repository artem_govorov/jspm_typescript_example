import angular from 'angular';
import application from 'app/app.ts';

/**
 * Manually bootstrap the application when AngularJS and
 * the application classes have been loaded.
 */
angular.element(document).ready(function () {
    console.log('Booting->' + application.name);
    let body = document.getElementsByTagName("body")[0];
    angular.bootstrap(body, [application.name], {strictDi: false})
});

