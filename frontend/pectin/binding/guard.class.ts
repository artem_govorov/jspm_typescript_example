export class Guard {
    private _guarded;

    // @deprecated use #runExclusive instead.
    execute<R>(func:()=>R) {
        return this.runIgnoringOtherTriggers(func);
    }

    // @deprecated use #runExclusive instead.
    runIgnoringOtherTriggers<R>(func:()=>R):R {
        return this.runExclusive(func);
    }

    runExclusive<R>(func:()=>R):R {

        if (!this._guarded) {
            try {
                this._guarded = true;
                return func();
            } finally {
                this._guarded = false;
            }
        }

    }
}