import {assert} from '..//lang';

import {Disposable, IDisposable} from './disposable.class';
import {PropertyAdapter} from './propertyAdapter.class';
import {valueModel} from "./dsl";

export class Binder implements IDisposable {

    private _garbage:Disposable;

    constructor() {
        this._garbage = new Disposable();
    }

    onChangeOf(source) {
        assert.notNull(source);
        let self = this;
        return {
            invoke(func) {
                assert.notNull(func);
                self.registerDisposable(source.onValueChange(func));
            }
        }
    }

    bind(source) {
        assert.notNull(source);
        let self = this;
        return {
            toProperty: function (propertyName) {
                return {
                    on: function (target) {
                        self.registerDisposable(new PropertyAdapter(source, target, propertyName));
                    }
                }
            }
        }
    }


    onDispose(func: ()=>void): void {
        this._garbage.onDispose(func);
    }

    registerDisposable(funcOrDisposable) {
        return this._garbage.registerDisposable(funcOrDisposable)
    }

    dispose() {
        this._garbage.dispose();
    }


}