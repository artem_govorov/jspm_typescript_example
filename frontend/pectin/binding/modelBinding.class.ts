import {ValueModel, ValueHolder} from "../model/baseValueModels";
import {Converter} from "../model/converter.class";
import {Disposable, IDisposable} from "./disposable.class";
import {Guard} from "./guard.class";
import {assert} from "../lang";
import {Converters} from "../model/converters.class";
import {trace} from "../debug/debug.decorators";

export class ModelBinding<T,S> extends Disposable {

    private _guard = new Guard();
    private _left:ValueModel<T>;
    private _right:ValueModel<S>;
    private _converter:Converter<T,S>;

    // the "source" is on the left... as far as the converter is concerned.. this needs to be
    // make explicit to the toSource/fromSource match the left and right.
    protected constructor(left:ValueModel<T>, right:ValueModel<S>, converter?:Converter<T,S>) {
        super();
        this._left = assert.notNull(left);
        this._right = assert.notNull(right);
        this._converter = converter || Converters.identity();
        this.registerDisposable(this._left.onValueChange(() => this.writeRight()));
        this.registerDisposable(this._right.onValueChange(() => this.writeLeft()));
        this.onDisposeNullify('_left');
        this.onDisposeNullify('_right');
        this.onDisposeNullify('_converter');
    }


    // @trace
    writeLeft():this {
        this._guard.runIgnoringOtherTriggers(() => {
            this._left.value = this._converter.toSource(this._right.value);
        });
        return this;
    }

    // @trace
    writeRight():this {
        this._guard.runIgnoringOtherTriggers(() => {
            this._right.value = this._converter.fromSource(this._left.value);
        });
        return this;
    }

}