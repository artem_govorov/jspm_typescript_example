import {DelegatingValueModel} from './models';

// todo: make this a real annotation when they become available and put this
// stuff on the object prototype instead.

export function attribute(subject, attrName, backingModel = new DelegatingValueModel()) {

    let modelName = `${attrName}Model`;
    let backingModelName = `_${attrName}Model`;

    // subject._myAttrModel
    Object.defineProperty(subject, backingModelName, {
        value: backingModel,
        configurable: false,
        writable: false,
        enumerable: false
    });

    // subject.myAttrModel
    Object.defineProperty(subject, modelName, {
        get: () => {
            return subject[backingModelName];
        }
    });

    /**
     * value getter.
     * e.g. subject.myAttr
     */
    Object.defineProperty(subject, attrName, {
        get: () => subject[modelName].value,
        set: (value) => subject[modelName].value = value
    });

    return subject[modelName];

}