import * as _ from 'lodash';

import {lang, assert} from 'pectin/lang';
import {ValueModel} from 'pectin/models';
import {ValueHolder} from 'pectin/models';
import {DelegatingValueModel} from 'pectin/models';
import {AndValueModel} from 'pectin/model/booleanValueModel.decorator';
import {isEqualOrSame} from 'pectin/compare';
import {not} from "../binding/dsl";
import {Trace} from "../debug/trace.class";

// @see https://github.com/Microsoft/TypeScript/issues/5627#issuecomment-155955170
export interface IFormModelBase<S> {
    setSourceModel(sourceModel:ValueModel<S>);
    dispose():void;
    enableWhen(enabledModel:ValueModel<boolean>);
    disableWhen(disabledModel:ValueModel<boolean>);
    enabledModel:ValueModel<boolean>;
    enabled:boolean;
    visibleWhen(visibleModel:ValueModel<boolean>);
    visibleModel:ValueModel<boolean>;
    visible:boolean;
    dirtyModel:ValueModel<boolean>;
    dirty:boolean;
}

export function FormModelBaseImpl(targetClass) {

    targetClass.prototype.doRead = function () {
        return this._readProperty(this._sourceValue);
    };
    
    targetClass.prototype.doWrite = function (value) {
        if (!this._sourceValue) {
            lang.illegalState("Can't write to property when _sourceValue is null or undefined");
        }
        this._writeProperty(this._sourceValue, value);
    };

    targetClass.prototype.dispose = function () {
        this._sourceModelRegistration && this._sourceModelRegistration.dispose();
        this._sourceModelRegistration = null;
    };

    targetClass.prototype.setSourceModel = function (source:ValueModel) {
        this.dispose();
        this._sourceModel = source;
        if (this._sourceModel) {
            this._sourceModelRegistration = this._sourceModel.onValueChange((newValue, oldValue) => {
                this._handleSourceModelChange(newValue, oldValue);
            })
        }
    };

    targetClass.prototype._handleSourceModelChange = function (newSource, oldSource) {
        this.fireValueChange();
    };

    // const superCreateTransaction = targetClass.prototype.createTransaction;
    // targetClass.prototype.createTransaction = function (initialValue) {
    //     return superCreateTransaction.call(this, this._readProperty(newSource))
    // };

    Object.defineProperty(targetClass.prototype, '_sourceValue', {
        get: function () {
            return this._sourceModel ? this._sourceModel.value : undefined
        }
    });

    targetClass.prototype.setErrors = function (errorList) {
        this._errorList = _.isArray(errorList) ? errorList : [errorList];
    };
    targetClass.prototype.setError = function (error) {
        this._errorList = [error];
    };
    targetClass.prototype.clearErrors= function () {
        this._errorList = [];
    };

    Object.defineProperty(targetClass.prototype, 'errors', {
        get: function () {
            return this._errorList || [];
        }
    });

    Object.defineProperty(targetClass.prototype, 'error', {
        get: function () {
            return _.first(this.errors)
        }
    });

    Object.defineProperty(targetClass.prototype, '_enabledWhenDelegate', {
        get: function () {
            if (!this['__enabledWhenDelegate']) {
                this['__enabledWhenDelegate'] = new DelegatingValueModel<boolean>();
                this['__enabledWhenDelegate'].setDefault(true)
            }
            return this['__enabledWhenDelegate'];
        }
    });

    Object.defineProperty(targetClass.prototype, 'enabledModel', {
        get: function () {
            if (!this['_enabledModel']) {
                const models = [
                    assert.notNull(this['_enabledWhenDelegate']),
                    assert.notNull(this.form.enabledModel)];
                this['_enabledModel'] = new AndValueModel(models)
            }
            return this['_enabledModel'];
        }
    });
    Object.defineProperty(targetClass.prototype, 'enabled', {
        get: function() {
            return this['enabledModel'].value;
        }
    });

    targetClass.prototype.enableWhen = function (whenModel:ValueModel<boolean>) {
        this['_enabledWhenDelegate'].setDelegate(whenModel);
    };
    targetClass.prototype.disableWhen = function (whenModel:ValueModel<boolean>) {
        this['enableWhen'](not(whenModel));
    };
    targetClass.prototype.enabledWhen = targetClass.prototype.enableWhen;



    Object.defineProperty(targetClass.prototype, '_visibleWhenDelegate', {
        get: function () {
            if (!this['__visibleWhenDelegate']) {
                this['__visibleWhenDelegate'] = new DelegatingValueModel<boolean>();
                this['__visibleWhenDelegate'].setDefault(true)
            }
            return this['__visibleWhenDelegate'];
        }
    });

    Object.defineProperty(targetClass.prototype, 'visibleModel', {
        get: function () {
            if (!this['_visibleModel']) {
                const models = [
                    assert.notNull(this['_visibleWhenDelegate']),
                    assert.notNull(this.form.visibleModel)];
                this['_visibleModel'] = new AndValueModel(models)
            }
            return this['_visibleModel'];
        }
    });
    Object.defineProperty(targetClass.prototype, 'visible', {
        get: function() {
            return this['visibleModel'].value;
        }
    });

    targetClass.prototype.visibleWhen = function (whenModel:ValueModel<boolean>) {
        this['_visibleWhenDelegate'].setDelegate(whenModel);
    };

}