import * as _  from 'lodash';
import {PropertyValueModel} from 'pectin/models';
import {ValueModel} from "pectin/models";
import {lang, assert} from "pectin/lang";
import {Disposable} from "pectin/binding/disposable.class";
import {FormModelBaseImpl} from "./formModelBase.decorator";
import {IFormModelBase} from "./formModelBase.decorator";
import {FormModel} from "./formModel.class";
import {DirtyTracking} from "../model/dirtyTracking.decorator";
import {IHasDirtyTracking} from "../model/dirtyTracking.decorator";
import {Converter} from "../model/converter.class";
import {Converters} from "../model/converters.class";
import {trace} from "../debug/debug.decorators";

@DirtyTracking()
@FormModelBaseImpl
class AbstractFormValueModel<T,S> extends ValueModel<T> {

    private propertyName:string;
    private _form:FormModel;
    private _converter:Converter<T,S> = Converters.identity();

    constructor(name:string, form:FormModel) {
        super();
        this._form = form;
        this.propertyName = assert.notNull(name);
    }

    _readProperty(sourceValue) {
        return sourceValue ? this._converter.fromSource(sourceValue[this.propertyName]) : undefined;
    }
    
    _writeProperty(sourceValue, value) {
        this['_sourceValue'][this.propertyName] = this._converter.toSource(value);
    }

    get form() {
        return this._form;
    }

    convertedUsing(converter:Converter<T,S>):FormValueModel {
        this._converter = converter;
        return this;
    }
}

// @see https://github.com/Microsoft/TypeScript/issues/5627#issuecomment-155955170
interface AbstractFormValueModel<T,S> extends IFormModelBase<S>, IHasDirtyTracking {}

export class FormValueModel<T,S> extends AbstractFormValueModel<T,S> {

    constructor(propertyName:string, sourceModel:ValueModel<S>, form:FormModel) {
        super(propertyName, form);
        this.setSourceModel(sourceModel);
    }


   //  @trace
    checkpoint(): void {
        super.checkpoint();
    }

   //  @trace
    revert(): void {
        super.revert();
    }
}