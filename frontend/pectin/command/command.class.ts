import {lang, assert} from '../lang';
import {Pectin} from '../pectin.class';
import {attribute} from '../annotations';
import {valueOf} from '../binding/dsl';
import {DelegatingValueModel, ValueModel, ValueHolder} from "../model/baseValueModels";
import {traceIfDebugContext, debugContext} from "../debug/debug.decorators";


const EMPTY_CONTEXT = {};

/**
 * <button command="ctrl.forward"/>
 */
export class Command<P,R> {

    static create<P,R>(onExecute:(param?:P)=>R):Command<P, R> {
        let cmd = new Command<P,R>();
        cmd.onExecute = assert.isFunction(onExecute);
        return cmd;
    }

    private _param:P;
    private _context:any;
    private _enabledModel:DelegatingValueModel<boolean>;

    @debugContext
    private _busyModel:ValueHolder<boolean>;

    constructor() {
        this._enabledModel = new DelegatingValueModel();
        this._enabledModel.setDefault(true);
        this._busyModel = new ValueHolder(false);
        this._busyModel.onValueChange((value) => {
            console.log(`--> ${this.constructor.name}.busy = ${value}`);
        })
    }

    /**
     * @deprecated use #enableWhen instead.
     * @param valueModel
     * @returns {Command}
     */
    enabledWhen(valueModel):this {
        console.log(`deprecated, use enableWhen(..) instead`);
        this.enableWhen(valueModel);
        return this;
    }

    enableWhen(valueModel):this {
        this._enabledModel.setDelegate(valueModel);
        return this;
    }

    disableWhen(valueModel):this {
        this.enableWhen(valueOf(valueModel).isFalsey());
        return this;
    }

    get enabled() {
        return this._enabledModel.value;
    }

    get enabledModel():ValueModel<boolean> {
        return this._enabledModel.immutable();
    }

    get disabled():boolean {
        return !this.enabled;
    }

    get busy():boolean {
        return this._busyModel.value;
    }

    get busyModel():ValueModel<boolean> {
        return this._busyModel.immutable();
    }

    disableWhenBusy():this {
        return this.disableWhen(this.busyModel);
    }

    // this will eventually be getParam(): P for Command<P>.
    getParam():P {
        return this._param;
    }

    getContextParam(name, defaultValue = null) {
        return this._context[name] ? this._context[name] : defaultValue;
    }

    // this will eventually be execute(param:T = null) for Command<T>.
    //@traceIfDebugContext
    execute(param?:P, context = EMPTY_CONTEXT):Promise<R> {

        if (this.disabled) return Pectin.reject('command is disabled');

        try {
            this._param = param;
            this._context = context;
            this.beforeExecute();
            this._busyModel.value = true;
            return Pectin.get().resolve(this.onExecute(param, context)).then((result) => {
                return result;
            }).finally(() => {
                this.afterExecute();
                this._busyModel.value = false;
                this._context = null;
                this._param = null;
            });
        } finally {
            this._param = null;
        }
    }

    onExecute(param:P, context:any):R | Promise<R> {
        return lang.abstractMethod(this.constructor)();
    }

    beforeExecute(){}
    afterExecute(){}
}
