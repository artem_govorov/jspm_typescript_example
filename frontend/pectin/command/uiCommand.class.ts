import inspect from 'object-inspect';
import {assert,lang} from '../lang';
import {Command} from './command.class';
//import {DelegatingValueModel} from '../models';
//import {attribute} from '../annotations'

/**
 * <button ui-command="ctrl.forward"/>
 */
export class UiCommand<P,R> extends Command<P,R> {

    static create<P,R>(onExecute:(param?:P, context?:any)=>R):UiCommand<P, R> {
        let cmd = new UiCommand<P,R>();
        cmd.onExecute = assert.isFunction(onExecute);
        return cmd;
    }

    getEventObject() {
        return this.getContextParam('event');
    }

    beforeExecute() {
        let event = this.getContextParam('event');
        if (event) {
            // if (event.clientX == 0 && event.clientY == 0) {
            //     console.log(`trigger event->${inspect(event)}`);
            //     console.trace();
            // }
            event['preventDefault']();
            event['stopPropagation']();
        } else {
            // console.log(`no event for command`);
        }
    }
}
