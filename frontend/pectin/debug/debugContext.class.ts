//noinspection TypeScriptCheckImport
import inspect from 'object-inspect';
import * as _ from 'lodash';

import {assert} from "../lang";
const debugContextProperty = Symbol.for('pectin:debug:context');

export class DebugContext {

    static inject(propertyValue:any, propertyName:string, parentObject:any) {
        if (propertyValue) {
            propertyValue[debugContextProperty] = new DebugContext(parentObject, propertyName)
        }
    }

    static remove(target) {
        if (target && target[debugContextProperty]) {
            delete target[debugContextProperty]
        }
    }

    static get(target):any {
        return (target && target[debugContextProperty]) ? target[debugContextProperty] : new PoorMansDebugContext(target);
    }

    static withContext(target:any, func:(context:any)=>void) {
        const context = DebugContext.get(target);
        if (context) {
            func(context);
        }
    }

    static inspect(target) {
        if (_.isObject(target)) {
            if (_.isArray(target)) {
                return inspect(target.map(item => DebugContext.inspect(item).toString()))
            } else {
                if (_.isPlainObject(target)) {
                    return inspect(target);
                } else {
                    return DebugContext.get(target).toString();
                }
            }
        } else {
            return inspect(target);
        }
    }

    constructor(private parentObject:any, private propertyName:string) {}

    toString():string {
        return `${DebugContext.get(this.parentObject) || this.parentObject.constructor.name}.${this.propertyName}`;
    }
}

class PoorMansDebugContext {

    constructor(private target) {
    }

    toString():string {
        return this.target['toDebugString'] ? this.target['toDebugString']() : `${this.target.constructor.name}`;
    }

}