import angular from 'angular';
import {CommandDirective} from './directives/command.directive';
import {FormModelDirective} from './directives/formModel.directive';
import {CheckboxListModelDirective} from './directives/checkboxListModel.directive';

let module = angular.module('pectin.directives', []);

// todo, I need to invert these to be CommandDirective.install(module, 'name') so the directives
// know what they're called.
module.directive('command', CommandDirective);
module.directive('formModel', FormModelDirective);
module.directive('checkboxListModel', CheckboxListModelDirective);

export default module;