import {assert} from '../lang';
import {ComputedValueModel} from './baseValueModels';
import {ValueModel} from "./baseValueModels";

export class LookupValueModel<T,S> extends ComputedValueModel<T,S> {

    private _map:Map<S,T>;
    private _defaultValue:T;

    constructor(source:ValueModel<S>, defaultValue?:T) {
        super(source);
        this._map = new Map();
        this.setDefault(defaultValue);
    }

    setDefault(defaultValue:T):LookupValueModel<T,S>  {
        this._defaultValue = defaultValue;
        return this;
    }

    compute(value:S):T {
        return this._map.get(value) || this._defaultValue;
    }

    when(key:S, value:T):LookupValueModel<T,S> {
        this._map.set(key, value);
        return this;
    }

}