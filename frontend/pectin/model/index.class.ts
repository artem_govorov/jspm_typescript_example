import {Disposable, IDisposable} from "../binding/disposable.class";
import {ListModel} from "./baseListModels";
import {assert, lang} from "../lang";
import {HasValue, ChangeListener, AbstractHasValue} from "./abstractHasValue.class";
import {EventListenerList} from "./eventListenerList.class";
import {HasValueChangeListeners} from "./hasValueChangeListeners.class";


export interface Index<R> extends HasValue<R>, IDisposable {
    get(key: any): R;
}

class Indicies<T> {

    static 'for'<T>(listModel: ListModel<T>): Indicies<T> {
        let indicies = listModel['_indicies'];
        if (!indicies) {
            indicies = new Indicies<T>(listModel);
            listModel['_indicies'] = indicies;
        }
        return indicies;
    }

    private _indicies = {};

    constructor(private _listModel: ListModel<T>) {}

    getOrCreate<C extends IndexBase>(key: string, ctor: typeof C): C {
        let idx = this._indicies[key];
        if (!idx) {
            idx = new ctor(this._listModel, key);
            this._indicies[key] = idx;
        } else {
            if (!(idx instanceof ctor)) {
                lang.illegalState(`index of different type already exists, expected ${ctor.name}, got ${idx.constructor.name}`)
            }
        }
        return idx;
    }
}


abstract class IndexBase<T,R> extends AbstractHasValue<this> implements Index<R> {

    private _lookupCache: Map<any, T>;
    protected _indexKey: any;
    protected _listModel: ListModel<T>;

    constructor(listModel:ListModel<T>, indexKey:string) {
        super();
        this._listModel = listModel;
        this._indexKey = assert.notNull(indexKey);
        listModel.onValueChange(() => {
            this.fireValueChangeAfter(() => this.clearCache());
        });
    }

    doRead() {
        return this;
    }

    get(key: any): R {
        return this.lookupCache.get(key);
    }

    public clearCache() {
        this._lookupCache = null;
    }

    protected get lookupCache(): Map<any, R> {
        if (!this._lookupCache) {
            // console.log(`   building cache...`);
            this._lookupCache = new Map<any, R>();
            this._populateCache(this._listModel.values, this._lookupCache);
            // console.log(`   done... size=${this._lookupCache.size}`);
        }
        return this._lookupCache;
    }

    protected abstract _populateCache(values: T[], map: Map<any, R>);

}

export class ScalarIndex<T> extends IndexBase<T,T> {

    static getOrCreate<T>(listModel: ListModel<T>, key: string): ScalarIndex<T> {
        return Indicies.for(listModel).getOrCreate(key, ScalarIndex);
    }

    _populateCache(values: T[], map: Map<any, T>): void {
        // console.log(`--> _populateCache`);
        values.forEach(entity => {
            map.set(entity[this._indexKey], entity)
        });
        // console.log(`<-- `);
    }
}

export class ListIndex<T> extends IndexBase<T,T[]> {

    static getOrCreate<T>(listModel: ListModel<T>, key: string): ListIndex<T> {
        return Indicies.for(listModel).getOrCreate(key, ListIndex);
    }

    _populateCache(values: T[], map: Map<any, T>): void {
        // console.log(`--> _populateCache`);
        values.forEach(entity => {
            const key = entity[this._indexKey];
            let list = map.get(key);
            if (!list) {
                list = [];
                map.set(key, list)
            }
            list.push(entity);
        });
        // console.log(`<-- `);
    }
}
