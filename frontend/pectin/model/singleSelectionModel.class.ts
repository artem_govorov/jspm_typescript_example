import {assert} from '../lang';
import {ValueHolder} from './baseValueModels';
import {ListModel} from './baseListModels';

export class SingleSelectionModel<T> extends ValueHolder<T> {

    private listModel:ListModel<T>;

    constructor(listModel:ListModel<T>) {
        super();
        this.listModel = listModel;
        // clear the selection if the list model no longer contains
        // the selection.
        this.listModel.onValueChange((newValues) => {
            if (!this.listModel.includes(this.value)) {
                this.value = null;
            }
        })
    }

}
