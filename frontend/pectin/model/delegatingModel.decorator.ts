import {lang} from '../lang';
import {AbstractHasValue} from './abstractHasValue.class';
import {ValueHolder} from "./baseValueModels";
import {ValueModel} from "./baseValueModels";
import {ListModel} from "./baseListModels";
import {trace} from "../debug/debug.decorators";
import {Trace} from "../debug/trace.class";

// @see https://github.com/Microsoft/TypeScript/issues/5627#issuecomment-155955170
export interface IDelegatingModel<T> extends ValueModel<T> {
    setDelegate(delegate:ValueModel<T>)
    getDelegate():ValueModel<T>
    setDefault(defaultValue:T | ValueModel<T>)
    withDefault(defaultValue:T | ValueModel<T>): this
    dispose();
}

export interface IDelegatingListModel<T> extends ListModel<T> {
    setDelegate(delegate:ListModel<T>)
    getDelegate():ListModel<T>
    setDefault(defaultValue:T[] | ListModel<T>)
    withDefault(defaultValue:T[] | ListModel<T>): IDelegatingListModel<T>
    dispose();
}

export function DelegatingModel(targetClass) {

    targetClass.prototype.doWrite = function (value) {
       this._delegate.value = value;
    };
    // targetClass.prototype.doWrite = Trace.wrapMethod(targetClass.prototype.doWrite, 'doWrite', targetClass.prototype);

    targetClass.prototype.doRead = function () {
        let sourceValue = this.getSourceValue();
        return (sourceValue === null || sourceValue === undefined) ? this.getDefaultValue() : sourceValue;
    };

    targetClass.prototype.onSourceValueChanged = function (newValue, oldValue, source) {
        this._valuePriorToChange = oldValue;
        this.fireValueChange();
    };

    targetClass.prototype.getValuePriorToChange = function () {
        return this._valuePriorToChange;
    };

    targetClass.prototype.commitTransaction = function () {
        return this.value;
    };

    targetClass.prototype.delegateTo = function (delegate) {
        this.setDelegate(delegate);
    };

    targetClass.prototype.setDelegate = function (delegate) {
        this._valuePriorToChange = this.value;
        this.fireValueChangeAfter(() => {
            this._unTrackSource(this._delegate);
            this._delegate = delegate;
            this._trackSource(this._delegate);
        });
    };

    targetClass.prototype.getDelegate = function () {
        return this._delegate;
    };

    targetClass.prototype.getSourceValue = function () {
        return this._delegate ? this._delegate.value : null
    };

    targetClass.prototype.getDefaultValue = function () {
        return this._defaultDelegate ? this._defaultDelegate.value : null
    };

    targetClass.prototype.setDefault = function (defaultValueOrModel) {
        this._valuePriorToChange = this.value;
        this.fireValueChangeAfter(() => {
            this._unTrackSource(this._defaultDelegate);
            this._defaultDelegate = defaultValueOrModel instanceof AbstractHasValue ? defaultValueOrModel : new ValueHolder(defaultValueOrModel);
            this._trackSource(this._defaultDelegate);
        });

    };

    // targetClass.prototype.withDefault = function(defaultValue) {
    //     this.setDefault(defaultValue);
    //     return this;
    // }

}