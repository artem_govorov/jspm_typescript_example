import {valueOf, not} from '../binding/dsl';
import {ComputedValueModel} from '../models';
import {ReducingValueModel} from '../models';
import {ValueModel} from "../models";
import {InjectBooleanChainMethods} from "./booleanValueModel.decorator";
import {IBooleanValueModel} from "./booleanValueModel.decorator";


@InjectBooleanChainMethods
export class ComputedBooleanValueModel<S> extends ComputedValueModel<boolean, S> {
    constructor(source, fn:(value:S)=>boolean) {
        super(source, fn);
    }
}

export interface ComputedBooleanValueModel extends IBooleanValueModel {}

