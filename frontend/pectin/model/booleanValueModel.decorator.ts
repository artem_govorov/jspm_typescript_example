import {ValueModel} from "../models";
import {ReducingValueModel} from "../models";
import {not, valueModel} from "../binding/dsl";
import {ComputedValueModel} from "./baseValueModels";
import {lang} from "../lang";
import {trace} from "../debug/debug.decorators";

export interface IBooleanValueModel extends ValueModel<boolean> {
    and(other:ValueModel<boolean>|boolean):IBooleanValueModel;
    andNot(other:ValueModel<boolean>|boolean):IBooleanValueModel;
    or(other:ValueModel<boolean>|boolean):IBooleanValueModel;
    orNot(other:ValueModel<boolean>|boolean):IBooleanValueModel;
}

// not sure if there's an easier way to do this... but I'm implementing manually here.
export class AndValueModel extends ReducingValueModel<boolean, boolean> implements IBooleanValueModel {

    constructor(sources:ValueModel<boolean>[] = []) {
        super(sources);
    }

    ////@trace
    compute(values:boolean[]):boolean {
        return values.length ? values.reduce((a, b) => !!a && !!b) : false;
    }

    and(other:ValueModel<boolean>|boolean):IBooleanValueModel {
        return new AndValueModel([this, valueModel.wrap(other)]);
    }

    andNot(other:ValueModel<boolean>|boolean):IBooleanValueModel {
        return new AndValueModel([this, new NotValueModel(valueModel.wrap(other))])
    }

    or(other:ValueModel<boolean>|boolean):IBooleanValueModel {
        return new OrValueModel([this, valueModel.wrap(other)]);
    }

    orNot(other:ValueModel<boolean>|boolean) {
        return new OrValueModel([this, new NotValueModel(valueModel.wrap(other))])
    }
}

export class OrValueModel extends ReducingValueModel<boolean, boolean> implements IBooleanValueModel {

    constructor(sources:ValueModel<boolean>[] = []) {
        super(sources);
    }

    compute(values:boolean[]):boolean {
        return values.reduce((a, b) => !!a || !!b, false)
    }

    and(other:ValueModel<boolean>|boolean):IBooleanValueModel {
        return new AndValueModel([this, valueModel.wrap(other)]);
    }

    andNot(other:ValueModel<boolean>|boolean):IBooleanValueModel {
        return new AndValueModel([this, new NotValueModel(valueModel.wrap(other))])
    }

    or(other:ValueModel<boolean>|boolean):IBooleanValueModel {
        return new OrValueModel([this, valueModel.wrap(other)]);
    }

    orNot(other:ValueModel<boolean>|boolean):IBooleanValueModel {
        return new OrValueModel([this, new NotValueModel(valueModel.wrap(other))])
    }
}

export class NotValueModel extends ComputedValueModel<boolean, boolean> implements IBooleanValueModel {

    constructor(source:ValueModel<boolean>) {
        super(source);
        this.computeWhenSourceOrValueIsNull();
    }

    compute(value:boolean):boolean {
        return !value;
    }

    and(other:ValueModel<boolean>|boolean):IBooleanValueModel {
        return new AndValueModel([this, valueModel.wrap(other)]);
    }

    andNot(other:ValueModel<boolean>|boolean):IBooleanValueModel {
        return new AndValueModel([this, new NotValueModel(valueModel.wrap(other))])
    }

    or(other:ValueModel<boolean>|boolean):IBooleanValueModel {
        return new OrValueModel([this, valueModel.wrap(other)]);
    }

    orNot(other:ValueModel<boolean>|boolean):IBooleanValueModel {
        return new OrValueModel([this, new NotValueModel(valueModel.wrap(other))])
    }
}


export function InjectBooleanChainMethods(targetClass) {

    targetClass.prototype.and = function (other:ValueModel<boolean>|boolean):IBooleanValueModel {
        return new AndValueModel([this, valueModel.wrap(other)]);
    };

    targetClass.prototype.andNot = function (other:ValueModel<boolean>|boolean):IBooleanValueModel {
        return this.and(not(valueModel.wrap(other)));
    };

    targetClass.prototype.or = function (other:ValueModel<boolean>|boolean):IBooleanValueModel {
        return new OrValueModel([this, valueModel.wrap(other)]);
    };

    targetClass.prototype.orNot = function (other:ValueModel<boolean>|boolean):IBooleanValueModel {
        return this.or(not(valueModel.wrap(other)));
    };
}