// import angular from 'angular';
import {assert, lang} from 'pectin/lang';
import {mixin} from 'pectin/traits';
import * as _ from 'lodash';
//noinspection TypeScriptCheckImport
import inspect from 'object-inspect';

// Mixin traits into an Object (another trait)
function decorate(dest, ...decorators) {
    // console.log(`--> delegate(${dest.constructor.name})`);
    let props = {};
    for (let decorator of decorators) {
        lang.prototypeChain(decorator, Decorator).forEach(proto => {
            for (let name of Object.getOwnPropertyNames(proto)) {
                // console.log(`property->${name}`);
                const descriptor = Object.getOwnPropertyDescriptor(proto, name);
                // console.log(`${name} -> ${inspect(descriptor)}`);
                if (name != 'constructor') {
                    if (_.isFunction(descriptor.value)) {
                        props[name] = {
                            value: (...args) => descriptor.value.call(decorator, ...args)
                        };
                    } else {
                        if (_.isFunction(descriptor.get)) {
                            props[name] = {
                                get: () => decorator[name]
                            };
                        }
                        if (_.isFunction(descriptor.set)) {
                            console.log(`${name} -> ${inspect(descriptor)}`);
                            throw new Error('properties setters not implmented yet.')
                        }

                    }
                }
            }
        });
        // const proto = Object.getPrototypeOf(decorator);
        // console.log(`prototype->${proto.constructor.name}`);
    }
    // console.log(`properties->${inspect(props)}`);
    Object.defineProperties(dest, props);
    // console.log(`<-- delegate`);
    return dest;
}

const decoratorSymbol = '__decorator';//Symbol.for('pectin:decorator:decorator');
const decoratedObjectSymbol = '__decoratedObject'; //Symbol.for('pectin:decorator:decoratedObject');

export class Decorator<T> implements T {

    private __rawObject:T;

    static decorate(rawObject:T, decorator = null) {
        assert.notNull(rawObject);
        decorator = decorator || new this(rawObject);
        // see http://nickmeldrum.com/blog/decorators-in-javascript-using-monkey-patching-closures-prototypes-proxies-and-middleware
        const decoratedObject = Object.create(rawObject);
        decorate(decoratedObject, decorator);
        decoratedObject[decoratorSymbol] = decorator;
        return decoratedObject;
    }

    static undecorate(object, options: {deep: boolean} = {deep: true}) {
        if (object) {
            if (object[decoratorSymbol]) {
                const target = object[decoratorSymbol].__rawObject;
                return options.deep ? Decorator.undecorate(target, options) : target;
            } else {
                return object;
            }
        } else {
            return null;
        }
    }

    constructor(object:T) {
        this.__rawObject = assert.notNull(object);
    }

    get object():T {
        return this.__rawObject;
    }

}