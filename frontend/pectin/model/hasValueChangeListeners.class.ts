import * as _ from 'lodash';
import {lang, assert} from '../lang';
import {EventListenerList} from './eventListenerList.class';
import {IDisposable} from "../binding/disposable.class";
import {AbstractHasValue, HasValue} from "./abstractHasValue.class";
import {trace, traceIfDebugContext} from "../debug/debug.decorators";
import {Guard} from "../binding/guard.class";


export interface ChangeListener<T> {
    (newValue:T, oldValue:T, source:AbstractHasValue<T>):void
}

export interface BeforeChangeListener<T> {
    (currentValue:T, source:AbstractHasValue<T>):void
}


type Callback<T> = (newValue:T)=>void;

export interface ValueChangeTransaction<T> {
    // TODO: this isn't a real commit.. it's more of a fire change event.
    // TODO: it would be nice to make it a real commit, but that's mean refactoring
    // TODO: the computed value models and how they work.
    commit(): void;
    onCommit(callback:Callback):ValueChangeTransaction<T>;
}

class RootTransaction<T> implements ValueChangeTransaction<T> {

    private callbacks:Callback<T>[] = [];
    private _terminate:(initialValue:T)=>void;

    constructor(public initialValue:T) {
    }

    commit() {
        this._terminate(this.initialValue);
    }

    onCommit(callback: Callback): ValueChangeTransaction<T> {
        this.callbacks.push(callback);
        return this;
    }

    onTerminate(callback: (initialValue:T)=>void) {
        this._terminate = callback;
    }

    notifyCallbacks(newValue:T) {
        this.callbacks.forEach(cb => cb(newValue));
    }
}

class NestedTransaction<T> implements ValueChangeTransaction<T> {

    constructor(private rootTx:RootTransaction) {}

    commit(): void {
        // noop.
    }

    onCommit(callback: (newValue:T)=>any):ValueChangeTransaction<T> {
        this.rootTx.onCommit(callback);
        return this;
    }
}

export abstract class HasValueChangeListeners<T> {

    private __beforeValueChangeListeners:EventListenerList<BeforeChangeListener<T>>;
    private __valueChangeListeners:EventListenerList<ChangeListener<T>>;
    private __afterValueChangeListeners:EventListenerList<ChangeListener<T>>;
    private __rootTransaction: RootTransaction<T>;

    /**
     * Adds a value change listener to this model.
     * @param func
     * @returns {Disposable} returns a dispose instance that can be used to remove the listener
     * and free the memory reference.
     */
    onValueChange(func:ChangeListener):IDisposable {
        return this._valueChangeListeners.add(func);
    }

    /**
     * Adds a value change listener to this model.
     * @param func
     * @returns {Disposable} returns a dispose instance that can be used to remove the listener
     * and free the memory reference.
     */
    onBeforeValueChange(func:BeforeChangeListener) {
        return this._beforeValueChangeListeners.add(func);
    }

    /**
     * Adds a value change listener to this model.
     * @param func
     * @returns {Disposable} returns a dispose instance that can be used to remove the listener
     * and free the memory reference.
     */
    onAfterValueChange(func:ChangeListener) {
        return this._afterValueChangeListeners.add(func);
    }

    abstract getValuePriorToChange():T;

    getTransaction(): ValueChangeTransaction {
        if (!this.__rootTransaction) {
            this.__rootTransaction = this.createTransaction(this.getValuePriorToChange());
            this.__rootTransaction.onTerminate((initialValue:T) => {
                this.endTransaction(initialValue);
            });
            this._fireBeforeValueChange(this.__rootTransaction.initialValue);
            return this.__rootTransaction;
        } else {
            return new NestedTransaction(this.__rootTransaction);
        }
    }

    protected createTransaction(initialValue:T) {
        return new RootTransaction(initialValue);
    }

    protected endTransaction(initialValue:T) {
        const tx = this.__rootTransaction;
        this.__rootTransaction = null;
        const value = this.commitTransaction();
        tx.notifyCallbacks(value);
        this._fireValueChange(value, initialValue);
        this._fireAfterValueChange(value, initialValue);
    }

    public inChangeTransaction() {
        return !!this.__rootTransaction;
    }

    public isChanging() {
        return !!this.__rootTransaction;
    }

    protected abstract commitTransaction():T;

    private _fireBeforeValueChange(initialValue: T) {
        if (this.__beforeValueChangeListeners) {
            this.__beforeValueChangeListeners.fire(listener => listener(initialValue, this));
        }
    }

    private _fireValueChange(newValue: T, initialValue: T) {
        if (this.__valueChangeListeners) {
            this.__valueChangeListeners.fire(listener => listener(newValue, initialValue, this));
        }
    }

    private _fireAfterValueChange(newValue:T, initialValue: T) {
        if (this.__afterValueChangeListeners) {
            this.__afterValueChangeListeners.fire(listener => listener(newValue, initialValue, this));
        }
    }


    // Todo: make this @lazy(()=>new EventListenerList()) : please note memoize won't work here as we dont' want toe create it every time we fire if there's no listeners.
    get _valueChangeListeners() {
        if (!this.__valueChangeListeners) {
            this.__valueChangeListeners = new EventListenerList<ChangeListener<T>>();
        }
        return this.__valueChangeListeners;
    }

    get _beforeValueChangeListeners() {
        if (!this.__beforeValueChangeListeners) {
            this.__beforeValueChangeListeners = new EventListenerList<BeforeChangeListener<T>>();
        }
        return this.__beforeValueChangeListeners;
    }

    get _afterValueChangeListeners() {
        if (!this.__afterValueChangeListeners) {
            this.__afterValueChangeListeners = new EventListenerList<BeforeChangeListener<T>>();
        }
        return this.__afterValueChangeListeners;
    }

}