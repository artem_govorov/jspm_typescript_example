// import {ConstructorFunction} from "pectin/lang";
import "reflect-metadata";
import {ValueHolder, ValueModel} from "../pectin/model/baseValueModels";

export enum PlaceMetadata {
    AuthenticationType,
    Public,
    Authenticated
}

export enum VisitMetadata {
    RouteIdentifier,
    InitMethodName
}

const ROUTE_IDENTIFIER_MODEL = Symbol.for('place:visit:routeIdentifierModel');

export class VisitHelper {

    static getRouteIdentifierModel(visit: any): ValueModel<any> {
        if (!visit[ROUTE_IDENTIFIER_MODEL]) {
            visit[ROUTE_IDENTIFIER_MODEL] = new ValueHolder();
        }
        return visit[ROUTE_IDENTIFIER_MODEL];
    }

    static invokeInitMethod(visit): void {
        const methodName = Reflect.getMetadata(VisitMetadata.InitMethodName, visit);
        return _.isFunction(visit[methodName]) ? visit[methodName]() : true;
    }
}


export function Public(targetClass) {
    Reflect.defineMetadata(PlaceMetadata.AuthenticationType, PlaceMetadata.Public, targetClass)
}

export function Authenticated(targetClass) {
    Reflect.defineMetadata(PlaceMetadata.AuthenticationType, PlaceMetadata.Authenticated, targetClass)
}


export function RouteIdentifier(prototype: Object, propertyKey: string | symbol): PropertyDescriptor {
    Reflect.defineMetadata(VisitMetadata.RouteIdentifier, propertyKey, prototype.constructor);

    Object.defineProperty(prototype, propertyKey, {
        configurable: false,
        get: function () {
            return VisitHelper.getRouteIdentifierModel(this).value
        },
        set: function(value: any) {
            VisitHelper.getRouteIdentifierModel(this).value = value;
        }
    });

}

// make this a property decorator.
// class Blah {
//   @RouteParameter('aName', Converters.DATE)
//   date: Date;
// }
// export function RouteParameter(name: string) {
//     return Reflect.metadata(ROUTE_IDENTIFIER, name);
// }

export function OnInit(target: any, propertyName: string, descriptor: TypedPropertyDescriptor<Function>) {
    Reflect.defineMetadata(VisitMetadata.InitMethodName, propertyName, target)
}


// export interface UiComponentParams {
//     controller:ConstructorFunction,
//     template?:string,
//     templateUrl?:string
// }
// export function UiComponent(params:UiComponentParams) {
//
// }