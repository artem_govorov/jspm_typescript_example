var publicAssets = "./public/frontend";
var sourceFiles = "./frontend";
var testFiles = "./spec/javascripts/**/*_spec.js";

function src(path) {
    return sourceFiles + path;
}
function dest(path) {
    return publicAssets + (path || '');
}

module.exports = {
    publicAssets: publicAssets,
    baseUrl: sourceFiles,
    browserSync: {
        proxy: 'localhost:3000',
        files: [dest('/**/*'), testFiles],
        open: false
    },

    javascript: {
        src: [
            src("/app/**/*.js"), src("/app/**/*.ts"),
            src("/api_actions/**/*.ts"), src("/api_actions/**/*.js"),
            src("/components/**/*.ts"), src("/components/**/*.js"),
            src("/place/**/*.ts"), src("/place/**/*.js"),
            src("/pectin/**/*.ts")
        ],
        bundle: 'concert_manager.js',
        dest: dest()
    },

    jspm_packages: {
        baseDir: src(),
        src: [src("/config.js"), src("/jspm_packages/**/*")],
        dest: dest()
    },

    templates: {
        src: [
            src("/app/**/*.html"),
            src("/components/**/*.html")
        ],
        dest: src('/app/')
    },

    sass: {
        baseDir: src("/scss"),
        src: [
            src("/scss/**/*.scss"),
            src("/app/**/*.scss"),
            src("/components/**/*.scss")
        ],
        bundle: 'concert_manager.css',
        dest: dest(),
        settings: {
            //indentedSyntax: false, // Enable .sass syntax!
            imagePath: '/frontend/images' // Used by the image-url helper
        }
    },
    //css: {
    //    src: [sourceFiles + "/**/*.css"],
    //    dest: publicAssets + "/stylesheets"
    //},
    images: {
        src: src("/images/**"),
        dest: dest("/images")
    },

    fonts: {
        src: src("/fonts/**"),
        dest: dest("/fonts")
    },

    i18n: {
        src: "config/locales/*.yml"
        //dest: 'public/javascripts'
    }
};
