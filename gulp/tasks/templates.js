var gulp = require('gulp');
var templateCache = require('gulp-angular-templatecache');
//var browserSync = require('browser-sync');

var config = require('../config').templates;

gulp.task('templates', function () {
    return gulp.src(config.src)
        .pipe(templateCache({standalone:true}))
        .pipe(gulp.dest(config.dest));
        //.pipe(browserSync.reload({stream: true}));
});