var gulp = require('gulp');
var run = require('gulp-run');
var gulpSequence = require('gulp-sequence');
var changed    = require('gulp-changed');
var browserSync = require('browser-sync');

var config = require('../config.js').javascript;
var jspm = require('../config.js');
//var path = require('path');
//var DevBuilder = require('jspm-dev-builder');

var chalk = require('chalk');
var path = require('path');
var Builder = require('jspm').Builder;
var builder = new Builder(jspm.baseUrl, path.join(jspm.baseUrl, 'config.js'));

//
//var appDevBuilder = new DevBuilder({
//    jspm: require('jspm'), // so you can use your local version of jspm
//    expression: 'app', // path to your app's entry point
//    outLoc: path.join(config.dest, config.bundle), // where you want the output file
//    logPrefix: 'jspm-app', // put at the beginning of log messages from dev builder
//    buildOptions: {
//        sfx: false, // if the build should be self executing
//        // below options are passed straight through to the builder
//        // the values shown are the defaults
//        minify: false,
//        mangle: false,
//        sourceMaps: true,
//        lowResSourceMaps: false,
//    }
//});
//

gulp.task('build', function(cb) {
    return gulpSequence('build-assets', 'copy-jspm-libs', 'build-src', cb);
});

gulp.task('build-assets', function(cb) {
    return gulpSequence('sass', 'images', cb);
});

gulp.task('build-src', function(cb) {
    return gulpSequence('i18n', 'templates', 'build-jspm-bundle', cb);
});

gulp.task('build-jspm-bundle', function (cb) {
    // var command = 'jspm bundle app ' + config.dest + "/" + config.bundle + ' --inline-source-maps';
    // console.log('command=' + command);
    // return run('jspm bundle app ' + config.dest + "/" + config.bundle, {verbosity: 2}).exec(cb);
    // run(command,{verbosity: 2}).exec(cb);
    var buildStart = Date.now();
    builder.bundle('app', path.join('public/frontend', config.bundle), {sourceMaps: 'inline'})
        .then(function () {
            console.log('Build finished', chalk.red(Date.now() - buildStart));
            cb(null, true)
        })
        .catch(function (err) {
            console.log('Build error');
            console.log(err.stack || err);
            cb(err)
        })

});

gulp.task('copy-jspm-libs', function() {
    return gulp.src(jspm.jspm_packages.src, {base: jspm.jspm_packages.baseDir})
        .pipe(changed(jspm.jspm_packages.dest)) // Ignore unchanged files
        .pipe(gulp.dest(jspm.jspm_packages.dest));
});

gulp.task('i18n', function(cb) {
    // the settings for this task are in <root>/config/i18n-js.yml
    return run('rails i18n:js:export').exec(cb);
});