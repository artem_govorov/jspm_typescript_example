# Stolen from teh internets...
# @see http://stackoverflow.com/a/11470890/1248812
#
module Logging

  module Loggable

    extend ActiveSupport::Concern

    # todo: https://github.com/rocketjob/rails_semantic_logger/issues/17
    # include SemanticLogger::Loggable

    class MyLogger < SimpleDelegator
      def initialize(delegator)
        super(delegator)
      end

      def info(message, *args)
        super("#{message}: #{args.inspect}")
      end

      def debug(message, *args)
        super("#{message}: #{args.inspect}")
      end

      def warn(message, *args)
        super("#{message}: #{args.inspect}")
      end

      def error(message, *args)
        super("#{message}: #{args.inspect}")
      end

    end

    def logger
      @logger ||= MyLogger.new(Rails.logger)
    end

    module ClassMethods

      def trace_methods(*methods)
        methods.each do |method|
          # logger.trace("--> #{method}", params: )
          # #   rename exiting method
          # #   log entry with params
          # begin
          #   #   call old method
          #   logger.trace("<-- #{method}")
          # rescue StandardError => e
          #   logger.error(e)
          #   raise e
          # end
        end
      end

    end

  end
end