module ApiActions
    class Response

      attr_accessor :resource, :status

      def initialize(resource_definition)
        @resource_definition = resource_definition
      end

      def add_message(message)
        messages << message
      end

      def add_meta(key, value)
        Lang.illegal_state!("key #{key} already defined") if self.meta.key?(key)
        self.meta[key] = value
      end

      def meta
        @meta ||= {}
      end

      def messages
        @messages ||= []
      end

      def to_envelope
        payload = resource ? @resource_definition.serialize(resource) : nil
        {payload: payload, messages: @messages.as_json, meta: JsonUtils.jsonize_keys(@meta.as_json)}
      end

    end
end