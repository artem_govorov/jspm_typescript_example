module ApiActions
  class JsonFormat

    # Converts a token rails snake to case to json camel case.  This method also
    # preserves any leading underscore.  Thus _some_var will become _someVar. This
    # is currently used to represent private/meta-data in serialized objects.  Examples
    # being _object_type and _mutable_attributes.
    #
    # Please note that it doesn't handle the case of multiple leading underscores.
    #
    # @param key the key to convert.
    def self.format(key)
      string_key = key.to_s
      private_key = string_key.start_with?('_')
      formatted_key = private_key ? "_#{string_key[1..-1].camelize(:lower)}" : string_key.camelize(:lower)
      key.is_a?(Symbol) ? formatted_key.to_sym : formatted_key
    end

    def self.transform_hash_keys(value)
      case value
        when Array
          value.map { |v| self.transform_hash_keys(v) }
        when Hash
          Hash[value.map { |k, v| [self.format(k), self.transform_hash_keys(v)] }]
        else
          value
      end
    end

    # def self.transform_hash(value)
    #   case value
    #     when Array
    #       value.map { |v| self.transform_hash_keys(v) }
    #     when Hash
    #       Hash[value.map { |k, v| [self.format(k), self.transform_hash_keys(v)] }]
    #     else
    #       value
    #   end
    # end

    def self.transform_values(values)
      values.map { |v| self.format(v) }
    end

  end
end