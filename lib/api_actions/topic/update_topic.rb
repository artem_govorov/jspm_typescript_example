module ApiActions
  module Topic

    # only here to we can go define_topic :update without specifying the topic class.
    class UpdateTopic < ::ApiActions::Topic::ResourceActionTopic
    end

  end
end