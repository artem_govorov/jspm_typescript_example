module ApiActions::Template

  class RequestTemplate

    CLASS_TEMPLATE = <<-eos
  export class <%= class_name %> extends AbstractRequest<<%= return_type %>> {
    get httpMethod():string { return '<%= action_def.http_method %>'; }
    get errorCodes():[string] { return [<%= exception_code_array %>]; }
    get hasEmptyResponse() { return <%= virtual? %>; }

    <% exception_codes.each do |c| %>
    catch<%= c.camelize %>(callback:ErrorCallback):<%= namespace %>.<%= class_name %> { this.errorDispatcher.setErrorHandler('<%=c%>', callback); return this; }
    <% end %><% exception_codes.each do |c| %>
    spyOn<%= c.camelize %>(callback:ErrorCallback):<%= namespace %>.<%= class_name %> { this.errorDispatcher.addErrorSpy('<%=c%>', callback); return this; }
    <% end %>
  }
    eos

    attr_reader :action_def, :namespace, :type, :parent_type

    def initialize(action_def, namespace, type, parent_type)
      @action_def = action_def
      @namespace = namespace
      @type = type
      @parent_type = parent_type
    end

    def class_name
      "#{action_def.name}".camelize
    end

    def return_type
      if action_def.virtual?
        'void'
      else
        action_def.collection? ? "Array<#{type}>" : type
      end
    end

    def exception_codes
      action_def.exceptions.map { |n| n.to_s.camelize(:lower) }
    end

    def exception_code_array
      exception_codes.map { |c| "'#{c}'" }.join(',')
    end

    def action_handler_name
      action_def.name
    end

    def permitted_params
      action_def.create_handler.permitted_params.map { |n| n.to_s.camelize(:lower) }
    end

    def virtual?
      action_def.resource_definition.virtual?
    end

    def parameter_type
      # this needs to handle all DAO's such as the rule modification ones (deleteOne, deleteRemaining etc).
      # params = []
      # params << 'id' if action_def.member_action?
      # params += permitted_params
      # "{#{params.map { |p| "#{p}?:any" }.join(',')}}"
      'any'
    end

    def render_request_class
      ERB.new(CLASS_TEMPLATE).result(binding)
    end

    def render_action_method
      template = ACTION_TEMPLATES[action_def.name]
      ERB.new(template).result(binding) if template
    end

  end

end