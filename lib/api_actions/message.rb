module ApiActions
  class Message

    attr_reader :channel_ids, :topic, :payload

    def initialize(channel_ids, topic, json_payload)
      @channel_ids = channel_ids
      @topic = topic
      @timestamp = Time.now.in_time_zone
      @payload = json_payload
    end

    # todo: ugly...can't use JsonUtils.jsonize_keys as it'll bugger the already jsonized payload.
    def as_json(options = nil)
      {
          channelIds: @channel_ids,
          topic: @topic.to_s.camelize(:lower),
          timestamp: @timestamp.iso8601(9),
          payload: @payload,
      }
    end

  end
end