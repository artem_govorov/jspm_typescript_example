module ApiActions

  class Topics
    include Enumerable

    Entry = Struct.new(:name, :topic_class, :topic_instance)

    def initialize(resource_def)
      @resource_def = resource_def
      @topics = {}
    end

    def each(&block)
      # @see http://stackoverflow.com/a/16663139/1248812
      return enum_for(__method__) if block.nil?
      @topics.values.map(&:topic_instance).each { |topic| block.call(topic) }
    end

    def topic(name)
      Lang.illegal_state!("Topic '#{name}' doesn't exist. Did you mean #{@topics.keys.join(',')}") unless self.has_topic?(name)
      @topics[name.to_sym].topic_instance
    end

    def has_topic?(topic)
      !!@topics.key?(topic.to_sym)
    end

    def create_topic(name, topic_class)
      Lang.illegal_state!("Topic with name #{name} already exists") if self.has_topic?(name)
      @topics[name.to_sym] = Entry.new(name, topic_class, topic_class.new(name.to_sym, @resource_def))
    end

    def clone(resource_def)
      Topics.new(resource_def).tap do |topics|
        @topics.values.each { |entry| topics.create_topic(entry.name, entry.topic_class) }
      end
    end

  end

end