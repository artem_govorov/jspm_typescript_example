module ApiActions

  class ResourceDefinitionBase


    attr_reader :resource_config, :belongs_to_config, :controller, :action_definitions, :topics

    delegate :resource_name, :resource_class, :serializer_class, :identified_by,
             :has_one?, :virtual?, :attribute?, :publish?,
             to: :resource_config

    delegate :parent_name, :parent_class, :parent_accessor,
             to: :belongs_to_config

    delegate :topic, :has_topic?, :create_topic, to: :topics


    def initialize(controller, resource_config: nil, belongs_to_config: nil)
      @controller = controller
      @resource_config = resource_config
      @belongs_to_config = belongs_to_config
      @action_definitions = []
      @topics = Topics.new(self)
      @global_exception_callbacks = {}
    end

    def belongs_to?
      !!belongs_to_config
    end

    def belongs_to_association_name
      belongs_to_config.inverse_of || belongs_to_config.parent_name
    end

    def collection?
      !self.has_one?
    end

    def action_definition(name)
      action_definitions.find { |a| a.name.to_s == name.to_s }
    end

    def schema_key
      self.ensure_configured!
      bits = []
      bits << self.parent_class.name.demodulize.camelize(:lower) if self.belongs_to?
      bits << (self.has_one? ? self.resource_name : self.resource_name.pluralize)
      bits.join('/')
    end

    def inverse_of
      belongs_to_config.inverse_of || (belongs_to? ? resource_name.pluralize : resource_name)
    end

    def parent_param_id
      "#{parent_class.name.demodulize.underscore}_id"
    end

    def parent_identified_by
      self.belongs_to_config.identified_by
    end

    def member_url(id_token: nil, parent_id_token: nil)
      self.ensure_configured!
      bits = []
      if self.attribute?
        parent_name = self.parent_class.name.demodulize.underscore
        bits << parent_name.pluralize
        bits << (parent_id_token || ":#{parent_name}_id")
      end
      bits << (self.has_one? ? self.resource_name : self.resource_name.pluralize)
      unless self.has_one?
        bits << (id_token || ':id')
      end
      bits.join('/')
    end

    def collection_url(parent_id_token: nil)
      self.ensure_configured!
      bits = []
      if self.belongs_to? || self.attribute?
        parent_name = self.parent_class.name.demodulize.underscore
        bits << parent_name.pluralize
        bits << (parent_id_token || ":#{parent_name}_id")
      end
      bits << (self.has_one? ? self.resource_name : self.resource_name.pluralize)
      bits.join('/')
    end

    # Checks if this resource has a global collection
    def global_resource?
      false
    end

    def global_resource_definition
      nil
    end

    def controller_name
      self.controller.name.underscore[0..-12]
    end

    def controller_method_name_for(action_def)
      action_def.name
    end

    def get_schema
      {
          identified_by: self.identified_by,
          collection: self.collection?,
          has_parent: self.belongs_to?,
          parent_identified_by: belongs_to? && belongs_to_config.identified_by,
          virtual: self.virtual?, # this should really go into the action definitions and/or be called
          actions: self.action_definitions.map { |action_def| [action_def.name, action_def.get_schema] }.to_h,
          message_bus: {
              channel_identifier: self.collection_url(parent_id_token: ':parent_id'),
              topics: self.topics
          }
      }
    end

    def inject_routes(router_dsl)
      self.action_definitions.each { |action| action.inject_route(router_dsl) }
    end


    def validate!
      raise Exceptions::NilParameterError unless resource_name
      raise Exceptions::NilParameterError unless resource_class
      raise Exceptions::NilParameterError if serializer_class.nil?
    end

    def serialize(resource_or_collection)

      return nil if self.virtual?

      return resource_or_collection unless serializer_class

      resource_or_collection.respond_to?(:map) ?
          serialize_collection(resource_or_collection) :
          serialize_resource(resource_or_collection)

    end

    def serialize_resource(resource)
      serializer_class.serialize(resource, {})
    end

    def serialize_collection(collection)
      collection.map { |r| serialize_resource(r) }
    end

    def create_action_definition(name, action_handler_class, role)
      ActionDefinition.new(self, name, action_handler_class, role).tap do |definition|
        action_definitions << definition
        definition.inject_controller_method(controller)
      end
    end

    def add_global_exception_callback(exception_code_or_class, block)
      exception_code = exception_code_or_class.is_a?(Class) ?
          exception_code_or_class.name.demodulize.underscore.downcase.to_sym :
          exception_code_or_class.to_sym

      Lang.illegal_state!("Callback for #{exception_code} already registered.") if @global_exception_callbacks.key?(exception_code)
      @global_exception_callbacks[exception_code] = block
    end

    def global_exception_callback(exception_code)
      @global_exception_callbacks[exception_code]
    end

    protected

    def ensure_configured!
      Assert.present!(self.resource_name, "resource_name required for #{self.controller.name}")
      Assert.truthy!(self.parent_class, "belongs_to required for attribute resources for #{self.controller.name}") if self.attribute?
    end


  end

end