module ApiActions
  module Errors
    class BadRequest < Base
      def initialize(payload = {})
        super(400, payload)
      end
    end
  end
end