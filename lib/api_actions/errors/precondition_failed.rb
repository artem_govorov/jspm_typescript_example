module ApiActions
  module Errors
    class PreconditionFailed < Base
      def initialize(data={})
        super(412, data)
      end
    end
  end
end