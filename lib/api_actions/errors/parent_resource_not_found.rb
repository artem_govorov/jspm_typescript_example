module ApiActions
  module Errors
    class ParentResourceNotFound < Base
      def initialize(payload = {})
        super(410, payload)
      end
    end
  end
end