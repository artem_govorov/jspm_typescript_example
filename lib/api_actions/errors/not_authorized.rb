module ApiActions
  module Errors
    class NotAuthorized < Base
      def initialize(payload = {})
        super(403, payload)
      end
    end
  end
end