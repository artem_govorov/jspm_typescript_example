module ApiActions
  module Errors
    class NotAuthenticated < Base
      def initialize(payload = {})
        super(401, payload)
      end
    end
  end
end