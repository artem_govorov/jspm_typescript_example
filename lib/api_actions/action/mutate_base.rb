module ApiActions
  module Action

      class MutateBase < ApiActions::Action::MemberAction

        include ::ApiActions::Action::Concerns::RaceConditions

        http_method :put

        publish(true)

        def require_params(params)
          params.require(resource_name)
        end

        def load_resource!
          super
          verify_optimistic_lock_token!(self.resource)
        end

      end

  end
end