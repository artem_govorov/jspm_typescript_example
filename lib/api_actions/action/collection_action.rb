module ApiActions
  module Action

    class CollectionAction < ApiActions::Action::Base

      raises :parent_resource_not_found, if: ->(resource_def) { resource_def.belongs_to? }

      def self.route_url(resource_definition, parent_id_token: nil)
        resource_definition.collection_url(parent_id_token: parent_id_token)
      end

      def parent_resource
        raise 'Parent only allowed for when belongs_to has been specified for the controller' unless resource_definition.belongs_to?
         unless @parent_resource
           @parent_resource = self.parent_class.find_by(id: params[resource_definition.parent_param_id])
           raise_parent_resource_not_found! unless @parent_resource
         end
        @parent_resource
      end

    end

  end

end