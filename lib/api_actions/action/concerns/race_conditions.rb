module ApiActions
  module Action
    module Concerns

      module RaceConditions

        OPTIMISTIC_LOCK_TOKEN_HEADER = 'LA-OPTIMISTIC-LOCK-TOKEN'

        extend ActiveSupport::Concern

        included do
          raises :race_condition
        end

        def verify_optimistic_lock_token!(has_token)
          raise Exceptions::IllegalArgumentError.new("resource can't be nil") unless has_token
          raise_optimistic_lock_token_missing! unless header(OPTIMISTIC_LOCK_TOKEN_HEADER)
          raise_race_condition! if has_token.optimistic_lock_token_stale?(header(OPTIMISTIC_LOCK_TOKEN_HEADER))
        end

        def on_raise_race_condition
          publish(resource_definition.topic(:update).create_message(self.resource)) if resource_definition.has_topic?(:update)
        end

      end

    end
  end
end