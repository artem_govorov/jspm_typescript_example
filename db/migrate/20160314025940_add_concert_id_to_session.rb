class AddConcertIdToSession < ActiveRecord::Migration[5.0]
  def change
    add_reference :sessions, :concert, foreign_key: true
  end
end
