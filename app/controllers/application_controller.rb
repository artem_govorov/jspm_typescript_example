class ApplicationController < ActionController::Base

  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

  # TODO: all the security stuff.
  # See http://technpol.wordpress.com/2013/11/09/creating-the-base-rails-backend/
  # before_filter :intercept_html_requests

end
