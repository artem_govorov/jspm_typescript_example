module Api
  module V1
    module Auth
      module Action

        class DestroyUserAction < ApiActions::Action::Destroy

          CantDestroySelf = Class.new(ApiActions::Errors::ConflictBase)

          raises CantDestroySelf

          def destroy_resource(resource)
            raise_cant_destroy_self! if resource.id == current_user.id
            resource.destroy
          end

        end

      end
    end
  end
end