class Concert < Entity
  has_mutable_attributes :date

  has_many :sessions, class_name: 'Session', autosave: true, dependent: :destroy

  # validates_presence_of :date
  # validates_date :date, on_or_after: :today

  before_create :initialize_sessions

  def initialize_sessions
    self.sessions = [
        Session.new(name: 'Kids'),
        Session.new(name: 'Intermediate'),
        Session.new(name: 'Advanced')
    ] unless self.sessions.any?
  end

end
