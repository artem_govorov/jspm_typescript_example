module Auth

  class Role < ClassyEnum::Base
  end

  class Role::Commissioning < Role
  end

  class Role::ScheduleManagement < Role
  end

  class Role::UserManagement < Role
  end

  class Role::AlarmManagement < Role
  end

  class Role::SystemAdministration < Role
  end

end