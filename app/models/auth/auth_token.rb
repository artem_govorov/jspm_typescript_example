module Auth
  class AuthToken

    include ActiveModel::Serialization

    attr_reader :jwt

    def initialize(user)
      user_json = ::Auth::UserSerializer.serialize(user)
      payload = {iss: 'au.com.coolon.lightarchitect', sub: user.id, user: user_json}
      @jwt = ::Knock::AuthToken.new(payload: ApiActions::JsonFormat.transform_hash_keys(payload)).token
    end

  end
end