module Auth
  class AuthTokenSerializer < TypedSerializer

    attributes :jwt

  end
end