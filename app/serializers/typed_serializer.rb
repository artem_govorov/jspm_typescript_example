class TypedSerializer < BaseSerializer

  attribute :_object_type#, key: :@object_type
  attribute :_create_attributes#, key: :@create_attributes
  attribute :_mutable_attributes#, key: :@mutable_attributes

  def _object_type
    object.class.name
  end

  def _mutable_attributes
    # These tokens represent attribute names on the client side so we
    # need to convert them to json case.
    attrs = object.try(:mutable_attributes).try(:names) || []
    attrs.map { |key| ApiActions::JsonFormat.format(key) }
  end

  def _create_attributes
    # These tokens represent attribute names on the client side so we
    # need to convert them to json case.
    attrs = object.try(:create_attributes).try(:names) || []
    attrs.map { |key| ApiActions::JsonFormat.format(key) }
  end

end
