class ConcertSerializer < EntitySerializer

  attributes :date

  has_many :sessions, serializer: SessionSerializer

  def date
    object.date.try(:iso8601)
  end

end